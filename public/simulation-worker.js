onmessage = (e) => {
	const options = e.data.options;
	const cards = e.data.cards;

	const num_simulations = options.num_simulations || 5; //TODO: better simulation default

	// Progress will be updated every update_frequency simulations.
	// So an update_frequency of 100 will update every 100 simulations.
	// Defaults to 5% of the number of simulations.
	const update_frequency = options.update_frequency || (num_simulations * 0.05);

	let avg = 0;
	let i = 0;
	let next_update = update_frequency;
	for (; i < num_simulations; i++) {
		const milled_count = runSimulation(cards);
		avg = (avg * i + milled_count)/(i+1); //Cumulative Average

		if (i >= next_update) {
			postMessage({
				status: 'RUNNING',
				num_simulated: i,
				num_total: num_simulations,
			});
			next_update = i + update_frequency;
		}
	}

	postMessage({
		status: 'FINISHED',
		avg_milled: avg,
		num_simulated: i,
		num_total: num_simulations,
	});
}

function runSimulation(cards) {
	shuffleArray(cards);
	let mv_total = 0;

	let i = 0;
	for (; i < cards.length; i++) {
		mv_total += cards[i].mv;
		if (mv_total >= 20) break;
	}

	return i;
}

function shuffleArray (array) {
	for (let i = array.length - 1; i > 0; i--) {
		const j = Math.floor(Math.random() * (i + 1));
		const temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
}
